# Example/Test files for the GUIs for each instrument type

This directory contains small programs that only contain a single type
of GUI for a single instrument. The main goal is to be able to test
the GUI and the instrument driver this way.

All files look very similar and the hardware addresses are hardcoded
and most likely need to be adjusted for other use cases.

Any of these files can be used as a good starting point to build a HC
control app, although perhaps the other example files might be more
useful.