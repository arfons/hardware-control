Examples
========

We provide several examples applications that highlight some of the
functionality or just show the different controls that can be used for
the instrument driver we have.

If you have the same hardware you will have to update the instrument
address in the example code to run them. In case you want to run them
without any hardware connected, you should add `--dummy` to the
command line, which will start them in dummy mode.

To get logging output you can add `--console` to reroute the logging
error to the console (otherwise it will go into a file in the
directory you are running the code). You can also change the loglevel
to `--info` or if you want a lot more output to `--debug`.

