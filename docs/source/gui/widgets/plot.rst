Plot Widget
===========

hardware_control.gui.widgets.plot module
----------------------------------------

.. automodule:: hardware_control.gui.widgets.plot
   :members:
   :undoc-members:
   :show-inheritance:
